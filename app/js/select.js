/* GLOBAL variables - Defines variables and arrays required throughout code  */
var data;
var i, x, n;
var phoneStyles;
var phones = new Array(5);
var input = new Array();

/* HTTP REQUEST - Assigns request object and opens connection  */
var request = new XMLHttpRequest();
request.open('GET', './data/phones.json', true);

/* PHONES DATA - sends request to JSON file then receives and parses JSON  */
request.onload = function() {
    var deviceSummary;
    data = this.response;
    data = JSON.parse(data);
    deviceSummary = data[0].deviceSummary;
    phoneStyles = deviceSummary.length;
    if (request.status >= 200 && request.status < 400) {} else {
        console.log('error');
    }
    phonesArray(deviceSummary);
    getSpecs();
    update();
}
request.send();

/* UPDATES INFO - Runs through the final two functions only to update data onpage */
function update() {
    getSelection();
    phoneInfo();
};

/* POPULATES PHONES ARRAY - Stores all the data needed from JSON data in JS array */
function phonesArray(deviceSummary) {
    /* phones ARRAY - defines 2D phones array to handle data  */
    for (i = 0; i < phoneStyles; i++) {
        phones[i] = new Array(9);
    };
    for (i = 0; i < phoneStyles; i++) {
        phones[i][0] = data[0].groupName;
        phones[i][1] = data[0].rating;
        phones[i][2] = deviceSummary[i].displayDescription;
        phones[i][3] = deviceSummary[i].colourName;
        phones[i][4] = deviceSummary[i].memory;
        /* Uses regular price if no discount price  */
        phones[i][5] = deviceSummary[i].priceInfo.hardwarePrice.oneOffDiscountPrice.gross;
        if (phones[i][5] == null) {
            phones[i][5] = deviceSummary[i].priceInfo.hardwarePrice.oneOffPrice.gross;
        };
        /* Uses regular price if no discount price  */
        phones[i][5] = '£' + phones[i][5];
        phones[i][6] = deviceSummary[i].priceInfo.bundlePrice.monthlyDiscountPrice.gross;
        if (phones[i][6] == null) {
            phones[i][6] = deviceSummary[i].priceInfo.bundlePrice.monthlyPrice.gross;
        };
        /* Adds £ to price string */
        phones[i][6] = '£' + phones[i][6];
        /* Removes any / from start of string  */
        x = deviceSummary[i].merchandisingMedia[0].value;
        phones[i][7] = x.replace("/images","images");
        phones[i][8] = deviceSummary[i].colourHex;
    }
};

/* GETS SPECIFICATIONS - Gets all variations of colour and capacity from phones array and adds them to HTML page in the form of radio buttons */
function getSpecs() {
    var hex = new Array();
    var specs = new Array(2);
    specs[0] = new Array();
    specs[1] = new Array();
    var counter = 0;
    var m;
    var ignore = false;
    /* Stores unique colours and capacities in specs array */
    for (n = 0; n < 2; n++) {
        m = n + 3;
        /* Runs through all entries in phones array for colours and capacities */
        for (i = 0; i < phoneStyles; i++) {
            /* Checks to see if colour/capacity is already stored in specs array */
            for (x = 0; x < counter; x++) {
                if (specs[n][x] == phones[i][m]) {
                    ignore = true;
                };
            };
            /* Stores unique colour/capaity in specs array  */
            if (!ignore) {
                specs[n][counter] = phones[i][m];
                /* Adds hex code to hex array if new colour is stored in specs array */
                if (n == 0) {
                    hex[counter] = phones[i][8];
                };
                counter++;
            }
            ignore = false;
        };
        counter = 0;
    };
    addSpecs(specs, hex);
};

/* ADDS SPECIFICATIONS - Adds all buttons needed for colour and capacity choices */
function addSpecs(specs, hex) {
    var html = new Array();
    var checked;
    var specification = 'colour';
    /* Checks for colour and capacity */
    for (n = 0; n < 2; n++) {
        html[n] = '';
        /* On page load first available colour and capacity is checked */
        checked = 'checked';
        /* Runs through specs array and creates a string containing HTML for each spec button available */
        for (i = 0; i < specs[n].length; i++) {
            html[n] += '<div class="radio" id="' + specification + '-radio">';
            html[n] += '<div class="border" id="' + specification + '-border-' + i + '">';
            html[n] += '<input type="radio" name="' + specification + '" id="' + specification + '-' + i + '"  value="' + specs[n][i] + '" ' + checked + ' onclick="update()" />';
            html[n] += '<label for="' + specification + '-' + i + '" class="radio-label" id="radio-label-' + specification + '-' + i + '"><div class="gradient"></div></label>';
            html[n] += '</div> ';
            html[n] += '</div> ';
            checked = '';
        };
        /* Adds HTML for specs button inside colour-radio/capacity-radio div */
        document.getElementById(specification + '-radio').innerHTML = html[n];
        specification = 'capacity';
    };
    /* Adds correct hex code to colour spec button */
    for (i = 0; i < hex.length; i++) {
        document.getElementById("radio-label-colour-" + i).style.background = hex[i];
    };
    /* Removes GB from the JSON data before displaying memory info in capacity button */
    for (i = 0; i < specs[1].length; i++) {
        var gigabyte = specs[1][i].replace(/[^0-9]/g, '');
        document.getElementById('radio-label-capacity-' + i).innerHTML = '<p>' + gigabyte + '</p>';
    };
};

/* GETS SELECTION - Gets the specifications currently chosen on index.html */
function getSelection() {
    var specification = 'colour';
    var selected = new Array();
    /* Checks for colour and capacity */
    for (n = 0; n < 2; n++) {
        selected[n] = document.getElementsByName('' + specification + '');
        for (i = 0; i < selected[n].length; i++) {
            /* Makes all borders transparent and adds colour to the borders of only the chosen spec buttons */
            if (selected[n][i].checked) {
                for (x = 0; x < selected[n].length; x++) {
                    document.getElementById(specification + "-border-" + x).style.border = "2px solid transparent";
                };
                document.getElementById(specification + "-border-" + i).style.border = "2px solid #00596a";
                /* Stores selection in input array*/
                input[n] = selected[n][i].value;
                break;
            }
        }
        specification = 'capacity';
    };
};

/* PHONE INFO - Adds news phone infor to page */
function phoneInfo() {
    /* Checks selected specs in input against data in phones array to find phone with selected specifications*/
    for (i = 0; i < phoneStyles; i++) {
        if (phones[i][3] == input[0] && phones[i][4] == input[1]) {
            var selectedPhone = i;
        };
    };
    /* Adds image and data of matched phone to index.html */
    document.getElementById("phone-img").src = phones[selectedPhone][7];
    document.getElementById("phone-img").alt = 'A picture of iPhone 8 in ' + phones[selectedPhone][3];
    document.getElementById("phone").innerHTML = phones[selectedPhone][0];
    document.getElementById("rating").innerHTML = phones[selectedPhone][1];
    document.getElementById("description").innerHTML = phones[selectedPhone][2];
    document.getElementById("colour").innerHTML = phones[selectedPhone][3];
    document.getElementById("capacity").innerHTML = phones[selectedPhone][4];
    document.getElementById("one-off-price").innerHTML = phones[selectedPhone][5];
    document.getElementById("monthly-price").innerHTML = phones[selectedPhone][6];
    /* Gets phone rating and rounds number up to nearest integer  */
    var rating = Math.ceil(phones[selectedPhone][1]);
    var stars = "";
    /* Creates string containing a canvas element for each star in rating */
    for (x = 0; x < rating; x++) {
        stars += '<canvas id="star' + x + '"  width="20" height="20"></canvas>';
    };
    /* Adds canvas string inside rating element on index.html */
    document.getElementById('rating').innerHTML = stars;
    /* Draws star and adss to each canvas element that has just been added to index.html */
    for (x = 0; x < rating; x++) {
        var canvas = document.getElementById('star' + x),
            ctx = canvas.getContext('2d');
        ctx.fillStyle = '#ffd20a';
        ctx.beginPath();
        ctx.moveTo(0, 7);
        ctx.lineTo(7, 11);
        ctx.lineTo(4, 20);
        ctx.lineTo(10, 14);
        ctx.lineTo(16, 20);
        ctx.lineTo(13, 11);
        ctx.lineTo(20, 8);
        ctx.lineTo(12, 8);
        ctx.lineTo(10, 0);
        ctx.lineTo(8, 8);
        ctx.lineTo(0, 8);
        ctx.fill();
        ctx.strokeStyle = '#d99100';
        ctx.stroke();
    };
};

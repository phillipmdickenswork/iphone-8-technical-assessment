var gulp         = require('gulp');
var sass         = require('gulp-sass');
var browserSync  = require('browser-sync')
var sourcemaps   = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var uglify       = require('gulp-uglify');
var csso         = require('gulp-csso');
var imagemin     = require('gulp-imagemin');
var del          = require('del');
var htmlmin      = require('gulp-htmlmin');

// DEFAULT - reloads the browser whenever SCSS, HTML or JS files change
gulp.task('default', ['watch', 'sass']);

// WATCH - reloads the browser whenever SCSS, HTML or JS files change
gulp.task('watch', ['browserSync', 'sass'], function (){
    gulp.watch('app/scss/*.scss', ['sass']);
    gulp.watch('app/js/*.js', browserSync.reload);
    gulp.watch('app/*.html', browserSync.reload);
})

// SASS - converts Sass to CSS with gulp-sass
gulp.task('sass', function(){
    return gulp.src('app/scss/styles.scss')
      .pipe(sourcemaps.init())
      .pipe(sass())
      .pipe(autoprefixer(AUTOPREFIXER_BROWSERS))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('app/css'))
      .pipe(browserSync.reload({
          stream: true
      }))
});

// BROWSERSYNC - reloads pages on file save
gulp.task('browserSync', function() {
    browserSync.init({
        browser: ["chrome"],
        server: {
            baseDir: 'app'
        },
    })
})

// AUTOPREFIXERS Broswers supporters
const AUTOPREFIXER_BROWSERS = [
  'ie >= 10',
  'ie_mob >= 10',
  'ff >= 30',
  'chrome >= 34',
  'safari >= 7',
  'opera >= 23',
  'ios >= 7',
  'android >= 4.4',
  'bb >= 10'
];

// CCSO - compiles and minifies SASS
gulp.task('cssmin', function () {
  return gulp.src('./app/scss/styles.scss')
    // Compile SASS files
    .pipe(sass({
      outputStyle: 'nested',
      precision: 10,
      includePaths: ['.'],
      onError: console.error.bind(console, 'Sass error:')
    }))
    .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
    .pipe(csso())
    // Output
    .pipe(gulp.dest('./dist/css'))
});

// UGLIFY - minifies JS
gulp.task('jsmin', function() {
  return gulp.src('./app/js/**/*.js')
    // Minify the file
    .pipe(uglify())
    // Output
    .pipe(gulp.dest('./dist/js'))
});


// IMAGEMIN - optmizes images
gulp.task('imgmin', () =>
    gulp.src('app/images/desktop/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images/desktop/'))
);

// CLEAN - clears dist folder
gulp.task('clean', () => del(['dist']));



